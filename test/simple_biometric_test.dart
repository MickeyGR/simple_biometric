import 'package:flutter_test/flutter_test.dart';
import 'package:simple_biometric/enums.dart';
import 'package:simple_biometric/simple_biometric.dart';
import 'package:simple_biometric/simple_biometric_platform_interface.dart';
import 'package:simple_biometric/simple_biometric_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockSimpleBiometricPlatform
    with MockPlatformInterfaceMixin
    implements SimpleBiometricPlatform {
  @override
  Future<String?> getPlatformVersion() => Future.value('42');

  @override
  Future<BiometricStatusResult?> showBiometricPrompt(
      {String? title, String? description, String? cancelText}) {
    // TODO: implement showBiometricPrompt
    throw UnimplementedError();
  }

  @override
  Future<IOSBiometricType> getIOSBiometricType() {
    // TODO: implement getIOSBiometricType
    throw UnimplementedError();
  }

  @override
  Future<bool> isAndroidBiometricHardwareAvailable() {
    // TODO: implement isAndroidBiometricHardwareAvailable
    throw UnimplementedError();
  }
}

void main() {
  final SimpleBiometricPlatform initialPlatform =
      SimpleBiometricPlatform.instance;

  test('$MethodChannelSimpleBiometric is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelSimpleBiometric>());
  });

  test('getPlatformVersion', () async {
    SimpleBiometric simpleBiometricPlugin = SimpleBiometric();
    MockSimpleBiometricPlatform fakePlatform = MockSimpleBiometricPlatform();
    SimpleBiometricPlatform.instance = fakePlatform;

    expect(await simpleBiometricPlugin.getPlatformVersion(), '42');
  });
}
