package dev.atokatl.simple_biometric

import androidx.annotation.NonNull
import androidx.biometric.BiometricPrompt
import androidx.core.content.ContextCompat
import androidx.fragment.app.FragmentActivity
import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import android.content.Context
import androidx.biometric.BiometricManager


class SimpleBiometricPlugin : FlutterPlugin, MethodCallHandler, ActivityAware {
    private lateinit var channel: MethodChannel
    private var activity: FragmentActivity? = null
    private val CHANNEL = "simple_biometric"
    private var authenticationInProgress = false


    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        channel = MethodChannel(flutterPluginBinding.binaryMessenger, CHANNEL)
        channel.setMethodCallHandler(this)
    }

    override fun onDetachedFromEngine(binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)
    }

    // ActivityAware interface methods
    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        activity = binding.activity as? FragmentActivity
    }

    override fun onDetachedFromActivity() {
        activity = null
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
        onAttachedToActivity(binding)
    }

    override fun onDetachedFromActivityForConfigChanges() {
        onDetachedFromActivity()
    }

    override fun onMethodCall(call: MethodCall, result: Result) {
        when (call.method) {
            "getPlatformVersion" -> result.success("Android " + android.os.Build.VERSION.RELEASE)
            "showBiometricPrompt" -> {
                val args: Map<String, String> = call.arguments() ?: mapOf()
                val title = args["title"] ?: "Default Title"
                val description = args["description"] ?: "Default Description"
                val cancelText = args["cancelText"] ?: "Cancel"
                showBiometricPrompt(title, description, cancelText, result)
            }

            "isBiometricHardwareAvailable" -> {
                val isAvailable = isBiometricHardwareAvailable(activity!!)
                result.success(isAvailable)
            }

            else -> result.notImplemented()
        }
    }

    private fun showBiometricPrompt(
        title: String,
        description: String,
        cancelText: String,
        result: Result
    ) {

        var resultSent = false

        val executor = activity?.let { ContextCompat.getMainExecutor(it) }
        val biometricPrompt = activity?.let {
            BiometricPrompt(it, executor!!, object : BiometricPrompt.AuthenticationCallback() {
                override fun onAuthenticationError(errorCode: Int, errString: CharSequence) {
                    super.onAuthenticationError(errorCode, errString)
                    if (!resultSent) {
                        resultSent = true
                        when (errorCode) {
                            BiometricPrompt.ERROR_NO_BIOMETRICS -> result.success("NO_BIOMETRICS")
                            BiometricPrompt.ERROR_HW_NOT_PRESENT -> result.success("NO_HARDWARE")
                            BiometricPrompt.ERROR_LOCKOUT -> result.success("BIOMETRIC_LOCKOUT")
                            BiometricPrompt.ERROR_LOCKOUT_PERMANENT -> result.success("BIOMETRIC_LOCKOUT_PERMANENT")
                            else -> result.error("AUTH_ERROR", "$errString", null)
                        }
                    }
                }

                override fun onAuthenticationSucceeded(authResult: BiometricPrompt.AuthenticationResult) {
                    super.onAuthenticationSucceeded(authResult)
                    if (!resultSent) {
                        resultSent = true
                        result.success("AUTH_SUCCESS")
                    }
                }

                //TODO: Report each error any number of times
                override fun onAuthenticationFailed() {
                    super.onAuthenticationFailed()
                    if (!resultSent) {
                        resultSent = true
                        result.error("AUTH_FAILED", "Authentication failed", null)
                    }
                }
            })
        }

        val promptInfo = BiometricPrompt.PromptInfo.Builder()
            .setTitle(title)
            .setSubtitle(description)
            .setNegativeButtonText(cancelText)
            .build()

        biometricPrompt?.authenticate(promptInfo)
    }


    fun isBiometricHardwareAvailable(context: Context): Boolean {
        val biometricManager = BiometricManager.from(context)
        val res = biometricManager.canAuthenticate()
        return !(res == BiometricManager.BIOMETRIC_ERROR_NO_HARDWARE || res == BiometricManager.BIOMETRIC_ERROR_HW_UNAVAILABLE)
    }
}
