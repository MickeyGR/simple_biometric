import 'dart:io';

import 'package:flutter/material.dart';
import 'package:simple_biometric/simple_biometric.dart';
import 'package:simple_biometric/enums.dart';

void main() {
  runApp(const LoginApp());
}

class LoginApp extends StatelessWidget {
  const LoginApp({super.key});

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Biometric Login',
      home: LoginPage(),
    );
  }
}

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final TextEditingController _usernameController = TextEditingController();
  final TextEditingController _passwordController = TextEditingController();
  final SimpleBiometric _simpleBiometric = SimpleBiometric();
  bool _isBiometricAvailable = false;
  bool _biometricEnabled = false;
  String _biometricType = "";

  @override
  void initState() {
    super.initState();
    _initBiometricType();
  }

  Future<void> _initBiometricType() async {
    if (Platform.isAndroid) {
      setState(() {
        _biometricType = "";
      });
    }
    if (Platform.isIOS) {
      final value = await _simpleBiometric.getIOSBiometricType();
      setState(() {
        _biometricType = value == IOSBiometricType.faceId
            ? "Face Id"
            : value == IOSBiometricType.touchId
                ? "Touch ID"
                : "";
      });
    }
    await _checkBiometricAvailability();
  }

  Future<void> _checkBiometricAvailability() async {
    if (Platform.isAndroid) {
      final isBiometricAvailable =
          await _simpleBiometric.isAndroidBiometricHardwareAvailable();
      setState(() {
        _isBiometricAvailable = isBiometricAvailable;
      });
    }
    if (Platform.isIOS) {
      setState(() {
        _isBiometricAvailable = _biometricType.isNotEmpty;
      });
    }
  }

  Future<void> _authenticate() async {
    if (_biometricEnabled) {
      try {
        final result = await _simpleBiometric.showBiometricPrompt(
          title: 'Biometric Authentication',
          description: 'Authenticate using your biometric credential',
          cancelText: 'Cancel',
        );
        _handleAuthenticationResult(result);
      } catch (e) {
        //error
      }
    }
  }

  void _handleAuthenticationResult(BiometricStatusResult? result) {
    switch (result) {
      case BiometricStatusResult.authSuccess:
        debugPrint('Authentication successful');
        break;
      case BiometricStatusResult.noBiometrics:
        debugPrint('No biometrics available');
        break;
      case BiometricStatusResult.noHardware:
        debugPrint('Biometric hardware not available');
        break;
      case BiometricStatusResult.biometricLockout:
        debugPrint('Biometric lockout');
        break;
      case BiometricStatusResult.biometricLockoutPermanent:
        debugPrint('Biometric lockout permanent');
        break;
      case BiometricStatusResult.authFailed:
        debugPrint('Authentication failed');
        break;
      default:
        if (Platform.isIOS && _biometricType.isNotEmpty) {
          debugPrint('Authentication config unavailable');
          _showConfigBiometricDialog();
        }
        break;
    }
  }

  void _showConfigBiometricDialog() {
    if (context.mounted) {
      showDialog(
        context: context,
        builder: (context) => AlertDialog(
          title: const Text('No Biometrics Available'),
          content: const Text(
              'Please go to settings and set up your biometric authentication.'),
          actions: [
            TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: const Text('OK'),
            ),
          ],
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Biometric Login'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            TextField(
              controller: _usernameController,
              decoration: const InputDecoration(labelText: 'Username'),
            ),
            TextField(
              controller: _passwordController,
              decoration: const InputDecoration(labelText: 'Password'),
              obscureText: true,
            ),
            if (_isBiometricAvailable)
              CheckboxListTile(
                title: Text(
                    'Enable ${_biometricType.isNotEmpty ? _biometricType : "biometric"} access'),
                value: _biometricEnabled,
                onChanged: (value) {
                  setState(() {
                    _biometricEnabled = value!;
                  });
                },
              ),
            ElevatedButton(
              onPressed: _authenticate,
              child: const Text('Login'),
            ),
          ],
        ),
      ),
    );
  }
}
