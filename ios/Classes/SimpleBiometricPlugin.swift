import Flutter
import UIKit
import LocalAuthentication

public class SimpleBiometricPlugin: NSObject, FlutterPlugin {
    public static func register(with registrar: FlutterPluginRegistrar) {
        let channel = FlutterMethodChannel(name: "simple_biometric", binaryMessenger: registrar.messenger())
        let instance = SimpleBiometricPlugin()
        registrar.addMethodCallDelegate(instance, channel: channel)
    }

    public func handle(_ call: FlutterMethodCall, result: @escaping FlutterResult) {
        switch call.method {
        case "getPlatformVersion":
            result("iOS " + UIDevice.current.systemVersion)
        case "showBiometricPrompt":
            if let args = call.arguments as? [String: String],
               let title = args["title"],
               let description = args["description"],
               let cancelText = args["cancelText"] {
                authenticateUser(title: title, description: description, cancelText: cancelText, result: result)
            } else {
                result(FlutterError(code: "INVALID_ARGUMENTS", message: "Missing or invalid arguments", details: nil))
            }
        case "checkBiometricType":
            checkBiometricType(result: result)
        default:
            result(FlutterMethodNotImplemented)
        }
    }

    private func authenticateUser(title: String, description: String, cancelText: String, result: @escaping FlutterResult) {
        let context = LAContext()
        context.localizedCancelTitle = cancelText

        var authError: NSError?
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &authError) {
            let reason = description
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) { success, evaluateError in
                DispatchQueue.main.async {
                    if success {
                        result("AUTH_SUCCESS")
                    } else {
                        if let error = evaluateError as NSError? {
                            switch error.code {
                            case LAError.biometryNotEnrolled.rawValue:
                                result("NO_BIOMETRICS")
                            case LAError.biometryNotAvailable.rawValue:
                                result("NO_HARDWARE")
                            case LAError.biometryLockout.rawValue:
                                result("BIOMETRIC_LOCKOUT")
                            default:
                                result(FlutterError(code: "AUTH_FAILED", message: "Authentication failed", details: nil))
                            }
                        }
                    }
                }
            }
        } else {
            if let error = authError {
                if error.code == LAError.biometryLockout.rawValue {
                    result("BIOMETRIC_LOCKOUT")
                } else {
                    result("BIOMETRIC_UNAVAILABLE")
                }
            } else {
                result("BIOMETRIC_UNAVAILABLE")
            }
        }
    }

   private func checkBiometricType(result: @escaping FlutterResult) {
       let context = LAContext()

       // This call is necessary to update the value of `context.biometryType`
       context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil)

       if #available(iOS 11.0, *) {
           // For iOS 11 and later, use the biometryType property
           switch context.biometryType {
           case .none:
               result("NO_BIOMETRICS")
           case .touchID:
               result("TOUCH_ID")
           case .faceID:
               result("FACE_ID")
           @unknown default:
               result("UNKNOWN")
           }
       } else {
           // For iOS 10 and earlier, assume Touch ID if the device supports biometrics
           result(context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: nil) ? "TOUCH_ID" : "NO_BIOMETRICS")
       }
   }
}
