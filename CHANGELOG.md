## 0.0.6

- Added handling for `BIOMETRIC_LOCKOUT` and `BIOMETRIC_LOCKOUT_PERMANENT` in the `showBiometricPrompt` function for both Android and iOS.
- Updated the documentation to reflect the behavior of iOS and Android regarding biometric lockout.
- Formatted the code using `dart format`.
- Passed `dart analyze` with no issues.
- Updated README.md.

## 0.0.5

- Enabled "AUTH_FAILED" in the `showBiometricPrompt` function.
- Formatted the code using `dart format`.
- Passed `dart analyze` with no issues.
- Updated README.md.
- Improved the package description in `pubspec.yaml`.

## 0.0.4

- Updated README.md

## 0.0.3

- Modified `checkBiometricType` function in iOS to return the type of biometry available on the device by hardware, regardless of whether it is configured or not.
- Refactored example code to version 0.0.2.
- Edited README.md.

## 0.0.2

- Updated README.md with preliminary configurations for Android and iOS.

## 0.0.1

- Initial release of the Simple Biometric plugin.
- Added biometric authentication feature.
- Fixed issues with plugin_platform_interface dependency.
