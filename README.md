# Simple Biometric

A Flutter plugin for easy implementation of biometric authentication in your Flutter app.

## Features

- Simple and easy-to-use API for biometric authentication.
- Supports both Android and iOS platforms.
- Customizable authentication dialog with title, description, and cancel button text.

# Preliminary Configurations

To use the "simple_biometric" Flutter library, please follow these preliminary configurations for Android and iOS:

## For Android:
1. In the main Android file, add the following import and class:
   ```java
   import io.flutter.embedding.android.FlutterFragmentActivity;
   
   class MainActivity: FlutterFragmentActivity() {
   }
   ```
2. In the Android manifest, add the following permission:
   ```xml
   <uses-permission android:name="android.permission.USE_BIOMETRIC"/>
   ```

## For iOS:
1. In the `Info.plist` file, add the following keys and descriptions:
   ```xml
   <key>NSFaceIDUsageDescription</key>
   <string>This app requires facial authentication to access certain functionalities.</string>
   <key>NSTouchIDUsageDescription</key>
   <string>This app requires fingerprint authentication to access certain functionalities.</string>
   ```

## Usage

Import the package:

```dart
import 'package:simple_biometric/simple_biometric.dart';
```

### Android Specific: Check Biometric is available

Before attempting authentication, check if biometric hardware is available:

```dart

final isBiometricAvailable = await _simpleBiometric.isAndroidBiometricHardwareAvailable();

```

### iOS Specific: Check Biometric Type

On iOS, you can check the available biometric type (Face ID or Touch ID):

```dart

IOSBiometricType iosBiometricType = await SimpleBiometric.getIOSBiometricType();

if(iosBiometricType == IOSBiometricType.faceId) {
// Face ID is available
} else if (iosBiometricType == IOSBiometricType.touchId) {
// Touch ID is available
}
```

### Authenticate Using Biometric

To authenticate using biometric, call the `showBiometricPrompt` method with optional customization
parameters:

```dart
try{

    BiometricStatusResult? result = await SimpleBiometric.showBiometricPrompt(
     title: 'Biometric Authentication',
     description: 'Authenticate using your biometric credential',
     cancelText: 'Cancel',
    );

   switch (result) {
     case BiometricStatusResult.authSuccess:
       log('Authentication successful');
       break;
     case BiometricStatusResult.noBiometrics:
       log('No biometrics available');
       break;
     case BiometricStatusResult.noHardware:
       log('Biometric hardware not available');
       break;
     case BiometricStatusResult.authFailed:
       log('Authentication failed');
       break;
     case BiometricStatusResult.biometricLockout:
       // (iOS does not distinguish between temporary and permanent lockout; it only returns this status. Android only displays this message once and then shows "No biometrics available.")
       log('Biometric lockout');
       break;
     case BiometricStatusResult.biometricLockoutPermanent:
       // (Android only displays this message once and then shows "No biometrics available." iOS does not return this status.)
       log('Biometric lockout permanent');
       break;
     default:
       if (Platform.isIOS && _biometricType.isNotEmpty) {
         log('Authentication config unavailable');
         _showConfigBiometricDialog();
       }
       break;
   }
} catch (e) {
}
```

## Example App

For a complete sample app using Simple Biometric for authentication, see the [example directory](https://gitlab.com/MickeyGR/simple_biometric/-/tree/main/example).

## License

This project is licensed under the MIT License - see the LICENSE file for details.
