import 'simple_biometric_platform_interface.dart';
import 'enums.dart';

class SimpleBiometric {
  /// Gets the current platform version.
  /// Returns a [String] representing the platform version.
  Future<String?> getPlatformVersion() =>
      SimpleBiometricPlatform.instance.getPlatformVersion();

  /// Displays a biometric authentication dialog.
  /// [title] is the title of the dialog.
  /// [description] is the description of the dialog.
  /// [cancelText] is the text for the cancel button.
  /// Returns a [BiometricStatusResult] indicating the result of the biometric authentication.
  /// Possible values are:
  ///   - [BiometricStatusResult.authSuccess] if the authentication was successful.
  ///   - [BiometricStatusResult.authFailed] if the authentication failed.
  ///   - [BiometricStatusResult.noBiometrics] if no biometrics are available.
  ///   - [BiometricStatusResult.noHardware] if biometric hardware is not available.
  ///   - [BiometricStatusResult.biometricLockout] if biometric authentication is locked out due to too many attempts. (iOS does not distinguish between temporary and permanent lockout; it only returns this status. Android only displays this message once and then shows [BiometricStatusResult.noBiometrics].)
  ///   - [BiometricStatusResult.biometricLockoutPermanent] if biometric authentication is permanently locked out. (Android only displays this message once and then shows [BiometricStatusResult.noBiometrics]. iOS does not return this status.)
  Future<BiometricStatusResult?> showBiometricPrompt({
    String? title,
    String? description,
    String? cancelText,
  }) =>
      SimpleBiometricPlatform.instance.showBiometricPrompt(
        title: title,
        description: description,
        cancelText: cancelText,
      );

  /// Gets the available biometric authentication type on iOS, regardless of whether it is configured or not.
  /// Returns an [IOSBiometricType] representing the biometric authentication type.
  /// Possible values are:
  ///   - [IOSBiometricType.noBiometrics] if no biometric hardware is available.
  ///   - [IOSBiometricType.touchId] if Touch ID hardware is available.
  ///   - [IOSBiometricType.faceId] if Face ID hardware is available.
  ///   - [IOSBiometricType.unknown] if the biometric authentication type is unknown.
  /// This function checks the hardware availability of biometric authentication types on the device
  /// and returns the type of biometry available by hardware, regardless of whether it is configured or not.
  /// It provides compatibility with iOS versions prior to 11.0 by assuming Touch ID if the device supports biometric authentication.
  Future<IOSBiometricType> getIOSBiometricType() =>
      SimpleBiometricPlatform.instance.getIOSBiometricType();

  /// Checks if biometric hardware is available on the device.
  /// Returns a [bool] indicating whether biometric hardware is available.
  Future<bool> isAndroidBiometricHardwareAvailable() =>
      SimpleBiometricPlatform.instance.isAndroidBiometricHardwareAvailable();
}
