import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'enums.dart';
import 'simple_biometric_method_channel.dart';

abstract class SimpleBiometricPlatform extends PlatformInterface {
  /// Constructs a SimpleBiometricPlatform.
  SimpleBiometricPlatform() : super(token: _token);

  static final Object _token = Object();

  static SimpleBiometricPlatform _instance = MethodChannelSimpleBiometric();

  /// The default instance of [SimpleBiometricPlatform] to use.
  ///
  /// Defaults to [MethodChannelSimpleBiometric].
  static SimpleBiometricPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [SimpleBiometricPlatform] when
  /// they register themselves.
  static set instance(SimpleBiometricPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }

  Future<BiometricStatusResult?> showBiometricPrompt({
    final String? title,
    final String? description,
    final String? cancelText,
  }) {
    throw UnimplementedError('showBiometricPrompt() has not been implemented.');
  }

  Future<IOSBiometricType> getIOSBiometricType() {
    throw UnimplementedError('getIOSBiometricType() has not been implemented.');
  }

  Future<bool> isAndroidBiometricHardwareAvailable() {
    throw UnimplementedError(
        'isAndroidBiometricHardwareAvailable() has not been implemented.');
  }
}
