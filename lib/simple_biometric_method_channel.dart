import 'dart:io';

import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'enums.dart';
import 'simple_biometric_platform_interface.dart';

/// An implementation of [SimpleBiometricPlatform] that uses method channels.
class MethodChannelSimpleBiometric extends SimpleBiometricPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('simple_biometric');

  @override
  Future<String?> getPlatformVersion() async {
    final version =
        await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }

  @override
  Future<BiometricStatusResult?> showBiometricPrompt({
    final String? title,
    final String? description,
    final String? cancelText,
  }) async {
    try {
      final res = await methodChannel.invokeMethod<String>(
        'showBiometricPrompt',
        {
          'title': title,
          'description': description,
          'cancelText': cancelText,
        },
      );

      final event = res?.split(",") ?? [];
      if (event.isEmpty) return null;
      switch (event[0]) {
        case 'AUTH_SUCCESS':
          return BiometricStatusResult.authSuccess;
        case 'NO_BIOMETRICS':
        case 'BIOMETRIC_UNAVAILABLE':
          return BiometricStatusResult.noBiometrics;
        case 'NO_HARDWARE':
          return BiometricStatusResult.noHardware;
        case 'BIOMETRIC_LOCKOUT':
          return BiometricStatusResult.biometricLockout;
        case 'BIOMETRIC_LOCKOUT_PERMANENT':
          return BiometricStatusResult.biometricLockoutPermanent;
        default:
          return null;
      }
    } on PlatformException catch (e) {
      if (e.code == 'AUTH_FAILED') {
        return BiometricStatusResult.authFailed;
      } else {
        rethrow;
      }
    }
  }

  @override
  Future<IOSBiometricType> getIOSBiometricType() async {
    if (!Platform.isIOS) {
      throw PlatformException(
        code: 'PLATFORM_ERROR',
        message: 'This feature is only available on iOS.',
        details: null,
      );
    }
    final res = await methodChannel.invokeMethod<String>('checkBiometricType');
    switch (res) {
      case 'NO_BIOMETRICS':
        return IOSBiometricType.noBiometrics;
      case 'TOUCH_ID':
        return IOSBiometricType.touchId;
      case 'FACE_ID':
        return IOSBiometricType.faceId;
      case 'UNKNOWN':
        return IOSBiometricType.unknown;
      default:
        return IOSBiometricType.none;
    }
  }

  @override
  Future<bool> isAndroidBiometricHardwareAvailable() async {
    final res =
        await methodChannel.invokeMethod<bool>('isBiometricHardwareAvailable');
    return res ?? false;
  }
}
