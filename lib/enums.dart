enum BiometricStatusResult {
  authSuccess,
  authFailed,
  noBiometrics,
  noHardware,
  biometricLockout,
  biometricLockoutPermanent,
}

enum IOSBiometricType {
  noBiometrics,
  touchId,
  faceId,
  unknown,
  none,
}
